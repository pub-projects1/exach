#include <boost/asio/io_context.hpp>
#include <cstdlib>
#include <ftxui/component/component.hpp>
#include <ftxui/component/component_base.hpp>
#include <ftxui/component/component_options.hpp>
#include <ftxui/component/screen_interactive.hpp>
#include <ftxui/dom/elements.hpp>
#include <ftxui/dom/node.hpp>
#include <ftxui/screen/screen.hpp>
#include <thread>

#include "appstate.hpp"
#include "inputmodal.hpp"
#include "messagesview.hpp"
#include "sendview.hpp"
#include "server.hpp"
#include "serversview.hpp"

auto main() -> int {
    boost::asio::io_context ctx;
    AppState appState{
        .screen = ftxui::ScreenInteractive::Fullscreen(),
    };
    std::thread serverThread([&ctx] {
        Server server(ctx);
        ctx.run();
    });

    using namespace ftxui; // NOLINT
    auto serversView{ServersView::make(appState)};
    auto messagesView{MessagesView::make(appState)};
    auto sendView{SendView::make(appState)};
    auto inputModalView{InputModal::make(ctx, appState)};

    serversView |= Modal(inputModalView, &appState.showInputModal);

    auto layout = Container::Horizontal({
        serversView,
        Container::Vertical({
            messagesView,
            sendView,
        }) | flex,
    });

    appState.screen.Loop(layout);
    serverThread.join();
    return EXIT_SUCCESS;
}
