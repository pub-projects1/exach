#pragma once

#include <boost/asio/io_context.hpp>
#include <cctype>
#include <ftxui/component/component.hpp>
#include <ftxui/component/component_base.hpp>
#include <ftxui/component/event.hpp>
#include <ftxui/component/loop.hpp>
#include <ftxui/dom/elements.hpp>

#include "appstate.hpp"
#include "client.hpp"

class InputModal : public ftxui::ComponentBase {
  public:
    InputModal(boost::asio::io_context &ctx, AppState &appState)
        : m{
              .appState = appState,
              .ctx = ctx,
          } {
        auto handleKeys = [this](ftxui::Event const &event) {
            if (ftxui::Event::Character('\n') == event) {
                m.appState.showInputModal = false;
                m.appState.clients.emplace_back(std::make_unique<Client>(m.ctx, m.server, m.appState));
                m.appState.servers.push_back(m.server);
                return true;
            }
            if (ftxui::Event::Escape == event) {
                m.appState.showInputModal = false;
                return true;
            }
            return false;
        };

        auto filterKeys = [](ftxui::Event const &event) {
            return event.is_character() && (std::isdigit(event.character()[0]) == 0) && event.character()[0] != '.';
        };

        Add({
            ftxui::Container::Vertical({
                ftxui::Renderer([] { return ftxui::text("Add: "); }),
                ftxui::Input(&m.server) | ftxui::CatchEvent(filterKeys) | ftxui::CatchEvent(handleKeys),
            }) | ftxui::border,
        });
    }

    static auto make(boost::asio::io_context &ctx, AppState &appState) -> ftxui::Component {
        return std::make_shared<InputModal>(ctx, appState);
    }

  private:
    struct alignas(64) M {
        std::string server;
        AppState &appState;           // NOLINT
        boost::asio::io_context &ctx; // NOLINT
    } m;
};
