#pragma once

#include "appstate.hpp"
#include "constants.hpp"
#include <boost/asio/completion_condition.hpp>
#include <boost/asio/connect.hpp>
#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/basic_endpoint.hpp>
#include <boost/asio/ip/basic_resolver_results.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/read.hpp>
#include <boost/asio/write.hpp>
#include <memory>
#include <stdexcept>
#include <string_view>

class Client {
  public:
    explicit Client(boost::asio::io_context &ctx, std::string_view const &serverString, AppState &appState)
        : m{
              .ctx = ctx,
              .resolver = boost::asio::ip::tcp::resolver(ctx),
              .serverString = serverString,
              .connection = TcpConnection::make(ctx, appState),
          } {
        resolve([this](auto &&, auto &&results) {
            boost::asio::async_connect(m.connection->socket(), results,
                                       [connection = m.connection](auto &&, auto &&) { connection->read(); });
        });
    }

    auto write(std::string_view data) -> void { m.connection->write(data); }

  private:
    template<typename ResolveToken = boost::asio::default_completion_token_t<boost::asio::io_context::executor_type>>
    auto resolve(ResolveToken const &onResolve) -> void {
        m.resolver.async_resolve(m.serverString, SERVER_LISTEN_SERVICE,
                                 [onResolve](const boost::system::error_code &errorCode,
                                             boost::asio::ip::tcp::resolver::results_type const &results) {
                                     if (errorCode) {
                                         throw std::runtime_error(errorCode.what());
                                     }
                                     onResolve(errorCode, results);
                                 });
    }

    class TcpConnection : public std::enable_shared_from_this<TcpConnection> {
      public:
        using pointer = std::shared_ptr<TcpConnection>;

        static auto make(boost::asio::io_context &ctx, AppState &appState) -> pointer {
            return std::make_shared<TcpConnection>(ctx, appState);
        }

        explicit TcpConnection(boost::asio::io_context &ctx, AppState &appState)
            : m{
                  .socket = boost::asio::ip::tcp::socket(ctx),
                  .appState = appState,
              } {}

        auto write(std::string_view data) -> void {
            boost::asio::async_write(m.socket, boost::asio::buffer(data), [](auto &&errorCode, std::size_t) {
                if (errorCode) {
                    // TODO: Handle errors // NOLINT
                }
            });
        }

        auto read() -> void {
            boost::asio::async_read(m.socket, boost::asio::buffer(m.buffer), boost::asio::transfer_at_least(1),
                                    [self = shared_from_this()](auto &&errorCode, std::size_t bytesTransferred) {
                                        if (!errorCode) {
                                            self->m.appState.messages.emplace_back(
                                                self->m.buffer.data(), self->m.buffer.data() + bytesTransferred);
                                            self->m.appState.screen.Post(ftxui::Event::Custom);
                                            self->read();
                                        }
                                    });
        }

        auto socket() -> boost::asio::ip::tcp::socket & { return m.socket; }

      private:
        struct alignas(128) M {
            std::array<char, 1024> buffer;
            boost::asio::ip::tcp::socket socket;
            AppState &appState; // NOLINT
        } m;
    };

    struct alignas(128) M {
        boost::asio::io_context &ctx; // NOLINT
        boost::asio::ip::tcp::resolver resolver;
        std::string_view serverString;
        TcpConnection::pointer connection;
    } m;
};
