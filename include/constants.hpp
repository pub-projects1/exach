#pragma once

static constexpr auto MIN_SEVER_PANE_WIDTH{20};
static constexpr auto MAX_SEVER_PANE_WIDTH{40};
static constexpr auto MAX_SEND_PANE_HEIGHT{4};
static constexpr auto MIN_SEND_PANE_HEIGHT{3};
static constexpr auto SERVER_LISTEN_PORT{4444};
static constexpr auto SERVER_LISTEN_SERVICE{"4444"};
