#pragma once

#include <boost/asio.hpp>
#include <ftxui/component/component.hpp>
#include <ftxui/component/component_base.hpp>
#include <ftxui/component/component_options.hpp>
#include <ftxui/dom/deprecated.hpp>
#include <ftxui/dom/elements.hpp>

#include "appstate.hpp"
#include "client.hpp"

class SendView : public ftxui::ComponentBase {
  public:
    explicit SendView(AppState &appState) : m({.appState = appState}) {
        using namespace ftxui; // NOLINT

        auto handleKeys = [this](Event const &event) {
            // TODO: Should be able to select different clients // NOLINT
            if (Event::Character('\n') == event) {
                m.appState.clients.at(0)->write(m.message);
                m.message = "";
                return true;
            }
            return false;
        };

        Add(Input(&m.message, InputOption::Spacious()) | CatchEvent(handleKeys) | border);
    }

    static auto make(AppState &appState) -> ftxui::Component { return std::make_shared<SendView>(appState); }

  private:
    struct alignas(64) M {
        std::string message;
        AppState &appState; // NOLINT
    } m;
};
