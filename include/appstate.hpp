#pragma once

#include <ftxui/component/screen_interactive.hpp>
#include <memory>
#include <string>
#include <vector>

class Client;

struct alignas(128) AppState {
    ftxui::ScreenInteractive screen;
    std::vector<std::string> servers;
    std::vector<std::string> messages;
    std::vector<std::unique_ptr<Client>> clients;
    bool showInputModal; // TODO: Synchronize // NOLINT
};
