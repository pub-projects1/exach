#pragma once

#include <algorithm>
#include <boost/asio.hpp>
#include <ftxui/component/component.hpp>
#include <ftxui/component/component_base.hpp>
#include <ftxui/dom/deprecated.hpp>
#include <ftxui/dom/elements.hpp>

#include "appstate.hpp"
#include "constants.hpp"

class ServersView : public ftxui::ComponentBase {
  public:
    explicit ServersView(AppState &appState)
        : m({
              .searchButton = ftxui::Button({
                  .label = "Add",
                  .on_click = [this] { m.appState.showInputModal = true; },
              }),
              .addButton = ftxui::Button({
                  .label = "Search",
                  .on_click = [] {},
              }),
              .serversView = ftxui::Renderer([this] {
                                 // TODO(omar): Probably use a menu to choose between different servers
                                 ftxui::Elements addresses;
                                 std::ranges::transform(m.appState.servers, std::back_inserter(addresses),
                                                        [](auto &&address) { return ftxui::text(address); });
                                 return ftxui::vflow(addresses);
                             }) |
                             ftxui::border,
              .appState = appState,
          }) {
        Add(ftxui::Container::Vertical({
                ftxui::Container::Horizontal({
                    m.searchButton | ftxui::flex,
                    m.addButton | ftxui::flex,
                }),
                m.serversView,
            }) |
            ftxui::border | ftxui::size(ftxui::WIDTH, ftxui::LESS_THAN, MAX_SEVER_PANE_WIDTH) |
            ftxui::size(ftxui::WIDTH, ftxui::GREATER_THAN, MIN_SEVER_PANE_WIDTH));
    }

    static auto make(AppState &appState) -> ftxui::Component { return std::make_shared<ServersView>(appState); }

  private:
    struct alignas(64) M {
        ftxui::Component searchButton;
        ftxui::Component addButton;
        ftxui::Component serversView;
        AppState &appState; // NOLINT
    } m;
};
