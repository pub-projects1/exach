#pragma once

#include <algorithm>
#include <boost/asio/io_context.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/placeholders.hpp>
#include <boost/asio/read.hpp>
#include <boost/asio/write.hpp>
#include <boost/log/trivial.hpp>
#include <boost/system/detail/error_code.hpp>

#include "constants.hpp"

class Server {
  public:
    explicit Server(boost::asio::io_context &ctx)
        : m{
              .acceptor = boost::asio::ip::tcp::acceptor(
                  ctx, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), SERVER_LISTEN_PORT)),
              .ctx = ctx,
          } {
        startAccept();
    }

    auto startAccept() noexcept -> void {
        auto tcpConnection{TcpConnection::make(m.ctx, *this)};
        m.acceptor.async_accept(tcpConnection->socket(),
                                [this, tcpConnection](boost::system::error_code const &errorCode) {
                                    if (!errorCode) {
                                        m.clients.push_back(tcpConnection);
                                        tcpConnection->read();
                                    }
                                    startAccept();
                                });
    }

  private:
    class TcpConnection : public std::enable_shared_from_this<TcpConnection> {
      public:
        using pointer = std::shared_ptr<TcpConnection>;

        static auto make(boost::asio::io_context &ctx, Server &server) -> pointer {
            return std::make_shared<TcpConnection>(ctx, server);
        }

        explicit TcpConnection(boost::asio::io_context &ctx, Server &server)
            : m({
                  .socket = boost::asio::ip::tcp::socket(ctx),
                  .server = server,
              }) {}

        auto socket() -> boost::asio::ip::tcp::socket & { return m.socket; }

        void write(std::vector<char> data) {
            std::ranges::for_each(m.server.m.clients, [data = std::move(data)](auto &&client) {
                boost::asio::async_write(client->socket(), boost::asio::buffer(data), [](auto &&, auto &&) {});
            });
        }

        void read() {
            boost::asio::async_read(
                m.socket, boost::asio::buffer(m.buffer, m.buffer.size() - 1), boost::asio::transfer_at_least(1),
                [self = shared_from_this()](boost::system::error_code const &errorCode, std::size_t readBytes) {
                    if (!errorCode || errorCode == boost::asio::error::eof) {
                        std::vector<char> data(self->m.buffer.data(), self->m.buffer.data() + readBytes);
                        self->write(std::move(data));
                        self->read();
                    } else {
                        // BOOST_LOG_TRIVIAL(error) << errorCode.what();
                    }
                });
        }

      private:
        struct alignas(128) M {
            std::array<char, 1024> buffer;
            boost::asio::ip::tcp::socket socket;
            Server &server; // NOLINT
        } m;
    };

    struct alignas(128) M {
        boost::asio::ip::tcp::acceptor acceptor;
        std::vector<TcpConnection::pointer> clients;
        boost::asio::io_context &ctx; // NOLINT
    } m;
};
