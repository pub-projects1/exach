#pragma once

#include "appstate.hpp"
#include "ftxui/dom/elements.hpp"
#include <algorithm>
#include <ftxui/component/component.hpp>
#include <ftxui/component/component_base.hpp>
#include <ftxui/dom/deprecated.hpp>
#include <ftxui/dom/node.hpp>

class MessagesView : public ftxui::ComponentBase {
  public:
    constexpr explicit MessagesView(AppState &appState)
        : m{
              .appState = appState,
          } {
        Add(ftxui::Renderer([this] {
            ftxui::Elements messages;
            std::ranges::transform(m.appState.messages, std::back_inserter(messages),
                                   [](auto &&message) { return ftxui::paragraph(message); });
            messages.push_back(ftxui::text("") | ftxui::focus);
            return ftxui::window(ftxui::text("messages"),
                                 ftxui::vflow(messages) | ftxui::vscroll_indicator | ftxui::frame) |
                   ftxui::flex;
        }));
    }

    static auto make(AppState &appState) -> ftxui::Component { return std::make_shared<MessagesView>(appState); }

  private:
    struct M {
        AppState &appState; // NOLINT
    } m;
};
