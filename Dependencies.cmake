cmake_minimum_required(VERSION 3.28)

# MUST be done before call to 'project'
get_cmake_property(vars CACHE_VARIABLES)
foreach(var ${vars})
  get_property(
    helpString
    CACHE "${var}"
    PROPERTY HELPSTRING)
  if("${helpString}" MATCHES "No help, variable specified on the command line." OR "${helpString}" STREQUAL "")
    # to see the variables being processed
    # ~~~
    # message("${var}:STRING = [${${var}}]  --  ${helpString}") # uncomment
    # ~~~
    list(APPEND CL_ARGS "-D${var}:STRING=${${var}}")
  endif()
endforeach()

project(dependencies)
include(CheckIPOSupported)
include(ExternalProject)

set(CMAKE_STAGING_PREFIX "${CMAKE_CURRENT_BINARY_DIR}/staging")
list(APPEND CMAKE_FIND_ROOT_PATH "${CMAKE_STAGING_PREFIX}")
if((DEFINED CMAKE_CROSSCOMPILING) AND NOT ("${CMAKE_HOST_SYSTEM_NAME}" MATCHES "${CMAKE_SYSTEM_NAME}"))
  set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)
  set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
  set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
  set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
else()
  set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE BOTH)
  set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE BOTH)
  set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY BOTH)
  set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM BOTH)
endif()

if(NOT DEFINED TARGET_HOST)
  execute_process(
    COMMAND ${CMAKE_C_COMPILER} -dumpmachine
    OUTPUT_VARIABLE TARGET_HOST
    OUTPUT_STRIP_TRAILING_WHITESPACE)
endif()

if(${TARGET_HOST} MATCHES "^([^-]+)-([^-]+)-([^-]+)-([^-]+)$")
  string(REGEX REPLACE "^([^-]+)-([^-]+)-([^-]+)-([^-]+)$" "\\1" TARGET_ARCH ${TARGET_HOST})
  string(REGEX REPLACE "^([^-]+)-([^-]+)-([^-]+)-([^-]+)$" "\\2" TARGET_VENDOR ${TARGET_HOST})
  string(REGEX REPLACE "^([^-]+)-([^-]+)-([^-]+)-([^-]+)$" "\\3" TARGET_OS ${TARGET_HOST})
elseif(${TARGET_HOST} MATCHES "^([^-]+)-([^-]+)-([^-]+)$")
  string(REGEX REPLACE "^([^-]+)-([^-]+)-([^-]+)$" "\\1" TARGET_ARCH ${TARGET_HOST})
  string(REGEX REPLACE "^([^-]+)-([^-]+)-([^-]+)$" "\\2" TARGET_VENDOR ${TARGET_HOST})
  string(REGEX REPLACE "^([^-]+)-([^-]+)-([^-]+)$" "\\3" TARGET_OS ${TARGET_HOST})
elseif(${TARGET_HOST} MATCHES "^([^-]+)-([^-]+)$")
  string(REGEX REPLACE "^([^-]+)-([^-]+)$" "\\1" TARGET_ARCH ${TARGET_HOST})
  string(REGEX REPLACE "^([^-]+)-([^-]+)$" "\\2" TARGET_VENDOR ${TARGET_HOST})
  set(TARGET_OS "unknown")
endif()

list(
  APPEND
  CL_ARGS
  "-G${CMAKE_GENERATOR}"
  "-DCMAKE_TOOLCHAIN_FILE:FILEPATH=${CMAKE_TOOLCHAIN_FILE}"
  "-DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE}"
  "-DCMAKE_INSTALL_PREFIX:PATH=${CMAKE_STAGING_PREFIX}"
  "-DCMAKE_STAGING_PREFIX:PATH=${CMAKE_STAGING_PREFIX}"
  "-DCMAKE_FIND_ROOT_PATH:PATH=${CMAKE_STAGING_PREFIX}"
  "-DCMAKE_FIND_LIBRARY_SUFFIXES:STRING=${CMAKE_STATIC_LIBRARY_SUFFIX}:${CMAKE_SHARED_LIBRARY_SUFFIX}"
  "-DCMAKE_FIND_ROOT_PATH_MODE_PACKAGE:STRING=${CMAKE_FIND_ROOT_PATH_MODE_PACKAGE}"
  "-DCMAKE_FIND_ROOT_PATH_MODE_INCLUDE:STRING=${CMAKE_FIND_ROOT_PATH_MODE_INCLUDE}"
  "-DCMAKE_FIND_ROOT_PATH_MODE_LIBRARY:STRING=${CMAKE_FIND_ROOT_PATH_MODE_LIBRARY}"
  "-DCMAKE_FIND_ROOT_PATH_MODE_PROGRAM:STRING=${CMAKE_FIND_ROOT_PATH_MODE_PROGRAM}")

check_ipo_supported(RESULT IPO_SUPPORTED OUTPUT error)
if(IPO_SUPPORTED)
  message(STATUS "IPO / LTO enabled")
  list(APPEND CL_ARGS "-DCMAKE_INTERPROCEDURAL_OPTIMIZATION:BOOL=TRUE")
else()
  message(STATUS "IPO / LTO not supported: <${error}>")
endif()

set(OPENSSL_PREFIX ${CMAKE_CURRENT_BINARY_DIR}/openssl-prefix)
ExternalProject_Add(
  openssl
  URL https://github.com/openssl/openssl/releases/download/openssl-3.2.1/openssl-3.2.1.tar.gz
  URL_HASH SHA256=83c7329fe52c850677d75e5d0b0ca245309b97e8ecbcfdc1dfdc4ab9fac35b39
  DOWNLOAD_EXTRACT_TIMESTAMP True
  CONFIGURE_COMMAND
    "${OPENSSL_PREFIX}/src/openssl/Configure" "--prefix=${CMAKE_STAGING_PREFIX}"
    "--openssldir=${CMAKE_STAGING_PREFIX}/etc/ssl" "no-err" "no-apps" "no-weak-ssl-ciphers" "-DPEDANTIC" "-DNO_FORK"
    "$<IF:$<STREQUAL:${TARGET_OS},android>,linux,${TARGET_OS}>-${TARGET_ARCH}"
  BUILD_COMMAND make "-j${CMAKE_BUILD_PARALLEL_LEVEL}"
                "CC=${CMAKE_C_COMPILER} $<$<BOOL:${CMAKE_C_COMPILER_TARGET}>:--target=${CMAKE_C_COMPILER_TARGET}>"
  INSTALL_COMMAND make install_sw)

# ~~~
# set(BOTAN_PREFIX ${CMAKE_CURRENT_BINARY_DIR}/botan-prefix)
# ExternalProject_Add(
#   botan
#   URL https://botan.randombit.net/releases/Botan-3.3.0.tar.xz
#   URL_HASH
#     SHA256=368f11f426f1205aedb9e9e32368a16535dc11bd60351066e6f6664ec36b85b9
#   CONFIGURE_COMMAND
#     "${BOTAN_PREFIX}/src/botan/configure.py" "--prefix=${CMAKE_STAGING_PREFIX}"
#     "--os=${TARGET_OS}" "--cpu=${TARGET_ARCH}"
#     "--cc=$<LOWER_CASE:$<IF:$<STREQUAL:${CMAKE_CXX_COMPILER_ID},GNU>,gcc,${CMAKE_CXX_COMPILER_ID}>>"
#     "--cc-bin=${CMAKE_CXX_COMPILER}" "--ar-command=${CMAKE_AR}"
#     "--no-install-python-module" "--disable-shared-library" "--without-sphinx"
#     "--without-rst2man" "--disable-cc-tests"
#     "--cc-abi-flags=$<$<BOOL:${CMAKE_CXX_COMPILER_TARGET}>:--target=${CMAKE_CXX_COMPILER_TARGET}>"
#   BUILD_COMMAND make "-j${CMAKE_BUILD_PARALLEL_LEVEL}"
#   INSTALL_COMMAND make install)
# ~~~

# ~~~
# set(ICU_PREFIX ${CMAKE_CURRENT_BINARY_DIR}/icu-prefix)
# ExternalProject_Add(
#   icu
#   URL https://github.com/unicode-org/icu/releases/download/release-75-1/icu4c-75_1-src.tgz
#   URL_HASH SHA256=cb968df3e4d2e87e8b11c49a5d01c787bd13b9545280fc6642f826527618caef
#   CONFIGURE_COMMAND
#     ${ICU_PREFIX}/src/icu/source/configure --prefix=${CMAKE_STAGING_PREFIX} --host=${TARGET_HOST} --enable-static
#     --disable-dyload --enable-tools=no --enable-tests=no --enable-samples=no --with-data-packaging=static
#     --with-cross-build=${ICU_PREFIX}/src/icu/source
#     "CC=${CMAKE_C_COMPILER} $<$<BOOL:${CMAKE_C_COMPILER_TARGET}>:--target=${CMAKE_C_COMPILER_TARGET}>"
#     "CXX=${CMAKE_CXX_COMPILER} $<$<BOOL:${CMAKE_CXX_COMPILER_TARGET}>:--target=${CMAKE_CXX_COMPILER_TARGET}>"
#     "CXXFLAGS=${CMAKE_CXX_FLAGS} -fext-numeric-literals"
#   BUILD_COMMAND make -j${CMAKE_BUILD_PARALLEL_LEVEL}
#   INSTALL_COMMAND make install)
#
# ExternalProject_Add_Step(
#   icu build_host
#   DEPENDEES patch
#   DEPENDERS configure
#   WORKING_DIRECTORY ${ICU_PREFIX}/src/icu/source
#   COMMAND ./configure
#   COMMAND make -j${CMAKE_BUILD_PARALLEL_LEVEL})
# ~~~

set(BOOST_PREFIX ${CMAKE_CURRENT_BINARY_DIR}/boost-prefix)
ExternalProject_Add(
  boost
  URL https://github.com/boostorg/boost/releases/download/boost-1.85.0/boost-1.85.0-cmake.tar.xz
  URL_HASH SHA256=0a9cc56ceae46986f5f4d43fe0311d90cf6d2fa9028258a95cab49ffdacf92ad
  BINARY_DIR ${BOOST_PREFIX}/src/boost
  # DEPENDS icu
  CONFIGURE_COMMAND ${BOOST_PREFIX}/src/boost/bootstrap.sh --prefix=${CMAKE_STAGING_PREFIX}
                    # --with-icu=${CMAKE_STAGING_PREFIX}
  BUILD_COMMAND
    ./b2 --user-config=user-config.jam -j${CMAKE_BUILD_PARALLEL_LEVEL}
    target-os=$<IF:$<STREQUAL:${TARGET_OS},mingw32>,windows,${TARGET_OS}>
    $<$<STREQUAL:${TARGET_OS},mingw32>:threadapi=pthread> link=static runtime-link=static threading=multi
    variant=release --with-headers --with-log --with-program_options --with-system --prefix=${CMAKE_STAGING_PREFIX}
    toolset=$<LOWER_CASE:$<IF:$<STREQUAL:${CMAKE_CXX_COMPILER_ID},GNU>,gcc,${CMAKE_CXX_COMPILER_ID}>>-${TARGET_ARCH}
    install
  INSTALL_COMMAND "")

ExternalProject_Add_Step(
  boost configure_compiler
  DEPENDEES configure
  DEPENDERS build
  WORKING_DIRECTORY ${BOOST_PREFIX}/src/boost
  COMMAND
    echo
    "using $<LOWER_CASE:$<IF:$<STREQUAL:${CMAKE_CXX_COMPILER_ID},GNU>,gcc,${CMAKE_CXX_COMPILER_ID}>> : ${TARGET_ARCH} : ${CMAKE_CXX_COMPILER} : <compileflags$<ANGLE-R>${CMAKE_CXX_FLAGS} $<$<BOOL:${CMAKE_CXX_COMPILER_TARGET}>: --target=${CMAKE_CXX_COMPILER_TARGET} <linkflags$<ANGLE-R>--target=${CMAKE_CXX_COMPILER_TARGET}> $<SEMICOLON> "
    > user-config.jam)

list(APPEND FTXUI_COMPILE_OPTIONS)
if("${TARGET_OS}" STREQUAL "mingw32")
  list(APPEND FTXUI_COMPILE_OPTIONS "-DCMAKE_TRY_COMPILE_TARGET_TYPE:STRING=STATIC_LIBRARY"
       "-DCMAKE_CXX_FLAGS:STRING=${CMAKE_CXX_FLAGS} -municode" "-DCMAKE_C_FLAGS:STRING=${CMAKE_C_FLAGS} -municode")
endif()
set(FTXUI_PREFIX ${CMAKE_CURRENT_BINARY_DIR}/ftxui-prefix)
ExternalProject_Add(
  ftxui
  GIT_REPOSITORY https://github.com/ArthurSonzogni/FTXUI.git
  GIT_TAG v5.0.0
  GIT_SHALLOW TRUE
  CMAKE_CACHE_ARGS ${CL_ARGS} ${FTXUI_COMPILE_OPTIONS})
